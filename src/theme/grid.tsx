/* eslint-disable no-magic-numbers */

export const grid = {
	gutter: {
		horizontal: {
			'1x': 8,
			'2x': 16,
		},
		vertical: {
			'1x': 8,
			'2x': 16,
		},
	},
};

export default {};

export const RESPONSE_TREE = 'data';
export const DEFAULT_ORDER = 'ASC';
export const SECOND_ORDER = 'DESC';

export const ORDER_FIELD_NAME = 'sortCol';
export const ORDER_DIR_NAME = 'sortDir';
export const PAGINATION = 'page';

export const QUERY_VARS = [ORDER_FIELD_NAME, ORDER_DIR_NAME, PAGINATION];
export default {};

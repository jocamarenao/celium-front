export default {};
const today = 'today';
const yesterday = 'yesterday';
const thisWeek = 'current_week';
const lastWeek = 'last_week';
const lastTwoWeeks = 'last_two_weeks';
const thisMonth = 'current_month';
const lastMonth = 'last_month';
// const lastThreeMonths = 'last_three_months';
// const lastSixMonths = 'last_six_months';
const currentYear = 'current_year';
const lastYear = 'last_year';
// const custom = 'custom';
export const DATE_RANGES = {
	today,
	yesterday,
	thisWeek,
	lastWeek,
	lastTwoWeeks,
	thisMonth,
	lastMonth,
	currentYear,
	lastYear,
	// custom,
};

export const PURCHASE_DATE = {
	expired: {
		days: 0,
		class: 'danger',
	},
	valid: {
		days: 5,
		class: 'info',
	},
	default: { class: 'warning' },
};

export const DATE_RANGE_FILTER = 'dateRange';
export const DATE_START_FILTER = 'startDate';
export const DATE_END_FILTER = 'endDate';
export const YMD_FORMAT = 'YYYY-MM-DD';
export const MDY_FORMAT = 'MMMM Do YYYY';
export const YMDHMS_FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const YY_FORMAT = 'YY';
export const V_CALENDAR_CONFIG = {
	formats: {
		title: 'MMMM YYYY',
		weekdays: 'W',
		navMonths: 'MMM',
		input: [YMD_FORMAT, 'YYYY-MM-DD', 'YYYY/MM/DD'],
		dayPopover: 'L',
	},
};

export const COUNTDOWN_UNITS = {
	days: 86400000,
	hours: 3600000,
	minutes: 60000,
	seconds: 1000,
};

export const MEMBERSHIP_REMAINING_DAYS = 10;
export const RENEW_MEMBERSHIP_DAYS = 0;

export const JANUARY = { key: 'january', value: '01' };
export const FEBRUARY = { key: 'february', value: '02' };
export const MARCH = { key: 'march', value: '03' };
export const APRIl = { key: 'april', value: '04' };
export const MAY = { key: 'may', value: '05' };
export const JUNE = { key: 'june', value: '06' };
export const JULY = { key: 'july', value: '07' };
export const AUGUST = { key: 'august', value: '08' };
export const SEPTEMBER = { key: 'september', value: '09' };
export const OCTOBER = { key: 'october', value: '10' };
export const NOVEMBER = { key: 'november', value: '11' };
export const DECEMBER = { key: 'december', value: '12' };

export const MONTHS = [
	JANUARY,
	FEBRUARY,
	MARCH,
	APRIl,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER,
];

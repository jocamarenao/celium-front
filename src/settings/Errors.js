/* eslint-disable no-magic-numbers */

export const SYSTEM_ALERT = [500];
export const BAD_REQUEST = [400];
export const AUTH = [401, 405];
export const UNPROCESSABLE = [422];
export const FORBIDDEN = [403, 503];
export const THROTTLE = [429];
export const NOT_FOUND = [404];
export const SUCCESS = [200, 201];

export const AUTH_ERRORS = [...AUTH, ...THROTTLE];
export const REDIRECT_ERRORS = [...NOT_FOUND, ...FORBIDDEN, ...AUTH_ERRORS];
export const COMMON_ERRORS = [...SYSTEM_ALERT];

export default {
	BAD_REQUEST,
	SYSTEM_ALERT,
	REDIRECT_ERRORS,
	COMMON_ERRORS,
	AUTH,
	AUTH_ERRORS,
	UNPROCESSABLE,
	FORBIDDEN,
	THROTTLE,
	NOT_FOUND,
};

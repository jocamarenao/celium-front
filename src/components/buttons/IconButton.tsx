import React from 'react';
import { Button, Icon, Tooltip } from 'antd';

type IconButtonProps = {
	tooltipTitle: string;
	dataCY: string;
	handleOnClick: () => void;
	buttonType?:
		| 'default'
		| 'link'
		| 'ghost'
		| 'primary'
		| 'dashed'
		| 'danger'
		| undefined;
	styles: object;
	buttonShape?: 'circle' | 'round' | 'circle-outline' | undefined;
	icon: string;
};

const IconButton = (props: IconButtonProps) => {
	const {
		tooltipTitle,
		dataCY,
		handleOnClick,
		buttonType = 'primary',
		styles,
		buttonShape = 'circle',
		icon,
	} = props;
	return (
		<Tooltip title={tooltipTitle}>
			<Button
				data-cy={dataCY}
				onClick={handleOnClick}
				type={buttonType}
				style={{ ...styles }}
				shape={buttonShape}
			>
				<Icon type={icon} />
			</Button>
		</Tooltip>
	);
};

IconButton.defaultProps = {
	buttonType: 'primary',
	buttonShape: 'circle',
};

export default IconButton;

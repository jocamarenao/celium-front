import React from 'react';

const SplashLoading = () => (
	<div className="splash-screen">
		<b style={{ fontSize: '30px' }}>Codeville</b>
		<p style={{ fontSize: '25px' }}>Wait a moment while we load your app.</p>
		<div className="loading-dot">.</div>
	</div>
);

export default SplashLoading;

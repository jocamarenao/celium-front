import { Redirect } from 'react-router-dom';
import React, { useContext } from 'react';

import UserContext from '../../context/UserContext';
import SplashLoading from '../SplashLoading/SplashLoading';

const NotFound: React.FC<{}> = (): JSX.Element => {
	const { isAuthenticated, loading } = useContext(UserContext);
	if (loading) return <SplashLoading />;
	let to = '/auth';
	if (isAuthenticated) to = '/home';
	return <Redirect to={to} />;
};

export default NotFound;

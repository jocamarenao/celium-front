import React, { useReducer, useEffect, useCallback } from 'react';
import moment from 'moment';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import UserContext from '../../context/UserContext';
import useLocalStorage from '../../hooks/useLocalStorage';
import Req from '../../util/AxiosRequest';
import { FETCH_USER } from '../../endpoints/endpoints';
import {
	ReducerState,
	ReducerAction,
	LoginPayload,
	UserContextProviderProps,
} from './types';

/* eslint-disable  @typescript-eslint/no-explicit-any */
const data: any = new Req();

const initialState = {
	isAuthenticated: false,
	user: {
		id: 0,
		role: 'none',
	},
	loading: true,
};

const reducer = (state: ReducerState, action: ReducerAction) => {
	switch (action.type) {
	case 'LOGIN':
		return {
			...state,
			isAuthenticated: true,
			loading: false,
			user: action.payload ? action.payload.user.data : initialState.user,
		};
	case 'LOGOUT':
		return {
			...state,
			loading: false,
			isAuthenticated: false,
			user: initialState.user,
		};
	case 'DONE_LOADING':
		return {
			...state,
			loading: false,
		};
	default:
		throw new Error();
	}
};

/* eslint-disable  @typescript-eslint/no-explicit-any */
const UserContextProvider = (
	props: RouteComponentProps<{}> & UserContextProviderProps,
) => {
	const [state, dispatch] = useReducer(reducer, initialState);
	const [set, get, remove] = useLocalStorage();
	// const checkTokenIn = 100;
	// const authInterval = () =>
	// 	setInterval(() => {
	// 		checkToken();
	// 	}, checkTokenIn);

	useEffect(() => {
		const { history } = props;
		// authInterval();
		history.listen(() => {
			checkToken();
		});
		checkToken(true);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// useEffect(() => {
	// 	console.log('useEffect2');
	// 	return () => {
	// 		// clearInterval(authInterval());
	// 	};
	// 	// eslint-disable-next-line react-hooks/exhaustive-deps
	// }, []);

	const fetchUser = useCallback(
		async () => data[FETCH_USER.method](FETCH_USER.endpoint()),
		[],
	);

	const checkToken = async (shouldFetchUser = false) => {
		try {
			const token = get('token', '');
			const tokenExpiresAt = get('tokenExpiresAt', '');
			if (token) {
				if (tokenExpiresAt && moment.utc().format() > tokenExpiresAt) {
					remove('token', '');
					remove('tokenExpiresAt', '');
					// clearInterval(authInterval());
					dispatch({ type: 'LOGOUT' });
				} else if (
					tokenExpiresAt
					&& moment.utc().format() < tokenExpiresAt
					&& shouldFetchUser
				) {
					const user = await fetchUser();
					dispatch({ type: 'LOGIN', payload: { isAuthenticated: true, user } });
				}
			} else if (!token) {
				dispatch({ type: 'LOGOUT' });
			}
		} catch (e) {
			dispatch({ type: 'LOGOUT' });
		}
	};

	const login = useCallback(
		async (payload: LoginPayload, history: any) => {
			try {
				set('token', payload.token);
				set('tokenExpiresAt', payload.tokenExpiresAt);
				const user = await fetchUser();
				dispatch({ type: 'LOGIN', payload: { isAuthenticated: true, user } });
				history.push('/home');
			} catch (e) {
				dispatch({ type: 'LOGOUT' });
			}
		},
		[fetchUser, set],
	);

	const logout = () => {
		remove('token', '');
		remove('tokenExpiresAt', '');
		dispatch({ type: 'LOGOUT' });
	};

	const { children } = props;

	return (
		<UserContext.Provider
			value={{
				...state,
				/* eslint-disable  @typescript-eslint/no-explicit-any */
				handleLogin: (payload: LoginPayload, history: any) => login(payload, history),
				handleLogout: () => logout(),
			}}
		>
			{children}
		</UserContext.Provider>
	);
};

export default withRouter(UserContextProvider);

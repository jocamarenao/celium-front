import React from 'react';
import { SessionType } from '../../types/userTypes';

export type ReducerState = {
	isAuthenticated?: boolean;
	user: SessionType;
	loading?: boolean;
};

export type ReducerActionPayloadUser = {
	data: SessionType;
};

export type ReducerActionPayload = {
	user: ReducerActionPayloadUser;
	isAuthenticated?: boolean;
	loading?: boolean;
};

export type ReducerAction = {
	type: string;
	payload?: ReducerActionPayload;
};

export type LoginPayload = {
	token: string;
	tokenExpiresAt: string;
};

export type UserContextProviderProps = {
	children: React.ReactNode;
};

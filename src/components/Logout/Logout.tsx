import React, { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../../context/UserContext';

const Logout: React.FC<{}> = (): JSX.Element => {
	const { handleLogout } = useContext(UserContext);
	useEffect(() => {
		if (handleLogout) handleLogout();
	});
	return <Redirect to="/" />;
};

export default Logout;

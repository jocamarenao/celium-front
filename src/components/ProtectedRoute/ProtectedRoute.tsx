import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import UserContext from '../../context/UserContext';
import AppLayout from '../../layouts/AppLayout/AppLayout';
import SplashLoading from '../SplashLoading/SplashLoading';
import { ProtectedRouteProps } from './types';
import './index.css';

const ProtectedRoute = (props: ProtectedRouteProps) => {
	const { component: Component, name, ...rest } = props;
	const { loading, isAuthenticated } = useContext(UserContext);
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	const renderComponent = (renderComponentProps: any): React.ReactNode => {
		if (loading) return <SplashLoading />;

		if (!isAuthenticated) return <Redirect to="/auth" />;

		return (
			<AppLayout name={name}>
				<Component {...renderComponentProps} />
			</AppLayout>
		);
	};
	return <Route render={renderComponent} {...rest} />;
};

export default ProtectedRoute;

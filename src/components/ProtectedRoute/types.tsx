export type ProtectedRouteProps = {
	component: React.ReactType;
	exact: boolean;
	path: string;
	name: string;
};

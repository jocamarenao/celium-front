import React from 'react';
import {
	Form, Row, Col, Input, Select,
} from 'antd';
import { TextProps } from './types';

const { Option } = Select;

const Text = (props: TextProps): JSX.Element => {
	const {
		handleSelectChange,
		handleInputChange,
		onBlur,
		errors,
		value,
		placeholder,
		name,
		prefix,
		label,
		type = 'text',
		options = [],
	} = props;
	return (
		<Form.Item
			label={label || ''}
			help={(errors && errors.getErrors(name)) || null}
			validateStatus={errors && errors.hasError(name) ? 'error' : ''}
		>
			<Row data-cy={`text-${name}`}>
				<Col span={24}>
					<>
						{type !== 'select' && (
							<Input
								type={type}
								id={name}
								name={name}
								data-cy={`input-${name}`}
								prefix={prefix}
								placeholder={placeholder || label}
								value={value}
								onBlur={onBlur}
								onChange={handleInputChange}
							/>
						)}
						{type === 'select' && (
							<Select
								id={name}
								data-cy={`select-${name}`}
								defaultValue=""
								value={value}
								onChange={(payload: string) => {
									if (handleSelectChange) handleSelectChange(name, payload);
									if (errors) errors.clearError(name);
								}}
							>
								<Option value="">Select option</Option>
								{options.map((item: Record<string, string | number>) => (
									<Option key={item.value} value={item.value}>
										{item.label}
									</Option>
								))}
							</Select>
						)}
					</>
				</Col>
			</Row>
		</Form.Item>
	);
};
export default Text;

import { Errors } from '../../types';

export type TextProps = {
	handleSelectChange?: (name: string, payload: string) => void;
	handleInputChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
	onBlur?: (event: React.ChangeEvent<HTMLInputElement>) => void;
	errors?: Errors;
	value: string | undefined;
	placeholder?: string;
	name: string;
	prefix?: string | React.ReactNode;
	label?: string;
	type?: 'text' | 'select' | 'password';
	options?: Record<string, string>[];
};

export type Errors = {
	getErrors: (name: string) => boolean;
	hasError: (name: string) => boolean;
	hasErrors: () => boolean;
	clearError: (name: string) => void;
};

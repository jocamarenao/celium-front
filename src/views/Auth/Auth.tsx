import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import AuthScene from './AuthScene';
import styles from './index.module.css';
import useAuthHooks from './useAuthHooks';

const Auth = (props: RouteComponentProps<{}>) => {
	const { history } = props;
	const { responseStatus, errors, handleSubmit } = useAuthHooks(history);
	return (
		<div className={styles['login-container']}>
			<div className={styles.login}>
				<AuthScene
					responseStatus={responseStatus}
					errors={errors}
					handleOnSubmit={handleSubmit}
				/>
			</div>
		</div>
	);
};

export default withRouter(Auth);

import { useContext, useEffect } from 'react';
import useFetch from '../../hooks/useFetch';
import { SUCCESS } from '../../settings/Errors';
import { LOGIN } from '../../endpoints/endpoints';
import UserContext from '../../context/UserContext';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const useAuthHooks = (history: any) => {
	const [loginResponse, , , errors, , , setLoginPayload] = useFetch(LOGIN);
	const { handleLogin } = useContext(UserContext);

	useEffect(() => {
		if (SUCCESS.includes(loginResponse.status)) {
			if (handleLogin) handleLogin(loginResponse.data, history);
		}
	}, [handleLogin, loginResponse, history]);

	return {
		responseStatus: loginResponse.status,
		errors,
		handleSubmit: setLoginPayload,
	};
};

export default useAuthHooks;

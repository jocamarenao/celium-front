export type Errortypes = {
	getErrors: (name: string) => boolean;
	hasError: (name: string) => boolean;
	hasErrors: () => boolean;
	clearError: (name: string) => void;
};

export type AuthFormValues = {
	email: string;
	password: string;
};

export type AuthSceneType = {
	responseStatus: number;
	errors: Errortypes;
	handleOnSubmit: (payload: AuthFormValues) => void;
};

export type UseAuthHooksType = {
	history: History;
};

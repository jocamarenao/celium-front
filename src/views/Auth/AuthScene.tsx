import React from 'react';
import {
	Button, Card, Icon, Row, Col, Alert,
} from 'antd';
import { Formik, Form } from 'formik';
import styles from './index.module.css';
import { BAD_REQUEST } from '../../settings/Errors';
import Text from '../../components/Text/Text';
import { AuthSceneType, AuthFormValues } from './types';

const AuthScene = (props: AuthSceneType) => {
	const { responseStatus, errors, handleOnSubmit } = props;

	const initialValues: AuthFormValues = { email: '', password: '' };

	return (
		<>
			{BAD_REQUEST.includes(responseStatus) && (
				<Alert
					data-cy="wrongCredentials"
					showIcon
					style={{ marginBottom: '10px' }}
					message="There was a problem"
					description="Incorrect sign in credentials."
					type="error"
					closable
				/>
			)}
			<Card>
				<Formik
					initialValues={initialValues}
					onSubmit={async (values) => {
						await handleOnSubmit(values);
					}}
				>
					{({
						values, handleChange, handleBlur, isSubmitting,
					}) => (
						<Form className={styles['login-form']}>
							<h1 className={styles['login-title']}>Sign in</h1>
							<Text
								prefix={
									<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
								}
								name="email"
								onBlur={(e) => {
									handleBlur(e);
									errors.clearError(e.currentTarget.name);
								}}
								handleInputChange={handleChange}
								value={values.email}
								errors={errors}
							/>
							<Text
								prefix={
									<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
								}
								name="password"
								onBlur={(e) => {
									handleBlur(e);
									errors.clearError(e.currentTarget.name);
								}}
								type="password"
								handleInputChange={handleChange}
								value={values.password}
								errors={errors}
							/>
							<Row style={{ marginBottom: '2%' }}>
								<Col span={24}>
									<a className={styles['login-form-forgot']} href="_blank">
										Forgot password
									</a>
								</Col>
							</Row>
							<Row style={{ marginBottom: '2%' }}>
								<Col span={24}>
									<Button
										id="signInButton"
										type="primary"
										htmlType="submit"
										className={styles['login-form-button']}
										disabled={errors.hasErrors() || isSubmitting}
									>
										Sign in
									</Button>
								</Col>
							</Row>
						</Form>
					)}
				</Formik>
			</Card>
		</>
	);
};

export default AuthScene;

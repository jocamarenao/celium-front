import { SessionType } from '../../types/userTypes';

export type UserFormValues = {
	firstName: string;
	lastName: string;
	username: string;
	email: string;
	role: string;
	password?: string;
	confirmPassword?: string;
};

export type userFiltersType = {
	firstName: string;
	username: string;
};

export type UserType = userFiltersType & {
	id: number;
	email: string;
	role: string;
	status: string;
	createdAt: string;
};

export type SorterType = {
	field: string;
	order: string;
};

export type handleAddUserType = {
	handleAddUser: () => void;
};

export type handleFiltersType = {
	handleFilter: (payload: userFiltersType) => void;
};

export type ListUsersTypes = {
	isLoading: boolean;
	dataSource: UserType[];
	user: SessionType | undefined;
	handleTableChange: (
		pagination: unknown,
		filters: unknown,
		sorter: SorterType
	) => void;
	handlePasswordReset: (id: number, role: string) => void;
	handleDeleteUser: (id: number, username: string) => void;
	handleUpdateUser: (id: number) => void;
};

export type ListUsersScreenTypes = ListUsersTypes &
	handleAddUserType &
	handleFiltersType;

export type ListUsersFiltersType = handleAddUserType & handleFiltersType;

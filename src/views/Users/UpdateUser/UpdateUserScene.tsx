import React from 'react';
import UserForm from '../components/UserForm';
import { CreateUserSceneType } from './types';

const CreateUserScene = (props: CreateUserSceneType) => {
	const {
		handleSubmit, errors, initialValues, getUserIsLoading,
	} = props;
	return (
		<UserForm
			loading={getUserIsLoading}
			action="update"
			handleOnSubmit={handleSubmit}
			errors={errors}
			initialValues={initialValues}
		/>
	);
};

export default CreateUserScene;

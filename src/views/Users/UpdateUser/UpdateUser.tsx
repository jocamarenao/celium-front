import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import UpdateUserScene from './UpdateUserScene';
import useUpdateUserHooks from './useUpdateUserHooks';

const UpdateUser = (props: RouteComponentProps<{ userID: string }>) => {
	const { history } = props;
	const { match } = props;
	const { params } = match;
	const { userID } = params;

	const [
		callUpdateUsers,
		updateUsersErrors,
		updateUserResponse,
		getUserIsLoading,
	] = useUpdateUserHooks(history, userID);
	return (
		<UpdateUserScene
			getUserIsLoading={getUserIsLoading}
			handleSubmit={callUpdateUsers}
			errors={updateUsersErrors}
			initialValues={updateUserResponse}
		/>
	);
};

export default withRouter(UpdateUser);

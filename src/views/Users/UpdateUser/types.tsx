import { Errors } from '../../../types';
import { UserFormValues } from '../types';

export type CreateUserSceneType = {
	errors: Errors;
	handleSubmit: (payload: UserFormValues) => void;
	initialValues: UserFormValues;
	getUserIsLoading: boolean;
};

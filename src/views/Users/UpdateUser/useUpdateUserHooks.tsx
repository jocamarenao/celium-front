import { useEffect, useCallback } from 'react';
import History from 'history';
import useFetch from '../../../hooks/useFetch';
import { SUCCESS } from '../../../settings/Errors';
import { GET_USER, UPDATE_USER } from '../../../endpoints/endpoints';
import Alert from '../../../util/Alert';
import { UserFormValues } from '../types';

const useCreateUserHooks = (
	history: History.History<{}> | string[],
	userId: string,
) => {
	const [getUserResponse, , , , setParam] = useFetch(GET_USER);
	const [
		updateUserResponse,,,

		updateUsersErrors,,,		,

		setUpdateUsersData,
	] = useFetch(UPDATE_USER);

	const alert = useCallback(() => new Alert(), []);

	useEffect(() => {
		setParam(userId);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		if (SUCCESS.includes(updateUserResponse.status)) {
			alert().toast('success', 'User updated successfully.', {});
			history.push('/users');
		}
	}, [alert, history, updateUserResponse]);

	const updateUser = (values: UserFormValues) => setUpdateUsersData(values, userId);

	return [
		updateUser,
		updateUsersErrors,
		getUserResponse.data,
		getUserResponse.isLoading,
	] as const;
};

export default useCreateUserHooks;

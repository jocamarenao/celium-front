import { Errors } from '../../../types';

export type UserFormValues = {
	id?: string;
	firstName: string;
	lastName: string;
	username: string;
	email: string;
	role: string;
	password?: string;
	confirmPassword?: string;
};

export type UserFormProps = {
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	handleSubmit?: (event: any) => void;
	handleOnSubmit?: (props: UserFormValues) => void;
	handleUpdateSubmit?: (props: UserFormValues, param: string | number) => void;
	values?: UserFormValues;
	initialValues?: UserFormValues;
	handleInputChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
	handleSelectChange?: (name: string, payload: string) => void;
	errors: Errors;
	action?: 'create' | 'update';
	loading?: boolean;
	placeholder?: string;
	name?: string;
	prefix?: string;
	label?: string;
	type?: 'text' | 'select' | 'password';
	options?: Record<string, string>[];
};

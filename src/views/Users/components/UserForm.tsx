import React from 'react';
import {
	Button, Row, Col, Card,
} from 'antd';
import { Formik, Form } from 'formik';
import Text from '../../../components/Text/Text';
import { roles } from '../../../settings/Users';
import Loading from '../../../components/Loading/Loading';
import { UserFormProps, UserFormValues } from './types';

const UserForm = (props: UserFormProps) => {
	const {
		initialValues,
		handleOnSubmit,
		errors,
		action = 'create',
		loading,
	} = props;

	const formikInitialValues: UserFormValues = {
		firstName: '',
		lastName: '',
		username: '',
		email: '',
		role: '',
		password: '',
		confirmPassword: '',
	};

	if (initialValues) {
		formikInitialValues.firstName =			initialValues.firstName || formikInitialValues.firstName;
		formikInitialValues.lastName =			initialValues.lastName || formikInitialValues.lastName;
		formikInitialValues.username =			initialValues.username || formikInitialValues.username;
		formikInitialValues.email =			initialValues.email || formikInitialValues.email;
		formikInitialValues.role = initialValues.role || formikInitialValues.role;
	}

	const roleOptions = (): Record<string, string>[] => roles.map((role) => ({ label: role, value: role }));

	const form = (
		<Formik
			initialValues={formikInitialValues}
			onSubmit={async (values) => {
				if (handleOnSubmit) await handleOnSubmit(values);
			}}
		>
			{({
				values, handleChange, setFieldValue, handleBlur, isSubmitting,
			}) => (
				<Form>
					<Text
						name="firstName"
						label="First name"
						placeholder="ex: jhon"
						value={values.firstName}
						onBlur={(e) => {
							handleBlur(e);
							errors.clearError(e.currentTarget.name);
						}}
						handleInputChange={handleChange}
						errors={errors}
					/>
					<Text
						name="lastName"
						label="Last name"
						placeholder="ex: smith"
						value={values.lastName}
						onBlur={(e) => {
							handleBlur(e);
							errors.clearError(e.currentTarget.name);
						}}
						handleInputChange={handleChange}
						errors={errors}
					/>
					<Text
						name="username"
						label="Username"
						placeholder="ex: jhon_smith"
						value={values.username}
						onBlur={(e) => {
							handleBlur(e);
							errors.clearError(e.currentTarget.name);
						}}
						handleInputChange={handleChange}
						errors={errors}
					/>
					<Text
						name="email"
						label="Email"
						placeholder="ex: jhon_smith@xxx.com"
						value={values.email}
						onBlur={(e) => {
							handleBlur(e);
							errors.clearError(e.currentTarget.name);
						}}
						handleInputChange={handleChange}
						errors={errors}
					/>
					<Text
						name="role"
						label="Role"
						type="select"
						options={roleOptions()}
						value={values.role}
						onBlur={(e) => {
							handleBlur(e);
							errors.clearError(e.currentTarget.name);
						}}
						handleSelectChange={setFieldValue}
						errors={errors}
					/>
					{action === 'create' && (
						<>
							<Text
								label="Password"
								type="password"
								name="password"
								value={values.password}
								onBlur={(e) => {
									handleBlur(e);
									errors.clearError(e.currentTarget.name);
								}}
								handleInputChange={handleChange}
								errors={errors}
							/>
							<Text
								label="Confirm password"
								type="password"
								name="confirmPassword"
								value={values.confirmPassword}
								onBlur={(e) => {
									handleBlur(e);
									errors.clearError(e.currentTarget.name);
								}}
								handleInputChange={handleChange}
								errors={errors}
							/>
						</>
					)}
					<Row style={{ marginBottom: '2%' }}>
						<Col span={24}>
							<Button
								data-cy="UserForm-Button"
								disabled={errors.hasErrors() || isSubmitting}
								style={{ float: 'right' }}
								type="primary"
								htmlType="submit"
							>
								{`${action === 'create' ? 'Create' : 'Update'} User`}
							</Button>
						</Col>
					</Row>
				</Form>
			)}
		</Formik>
	);

	return (
		<div style={{ marginLeft: '20%', marginRight: '20%' }}>
			<Card>{!loading ? form : <Loading />}</Card>
		</div>
	);
};

export default UserForm;

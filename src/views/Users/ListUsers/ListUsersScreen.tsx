import React from 'react';
import ListUsersFilters from './components/ListUsersFilters';
import ListUsersTable from './components/ListUsersTable';
import { ListUsersScreenTypes } from '../types';

const ListUsersScreen = (props: ListUsersScreenTypes) => {
	const {
		handleFilter,
		isLoading,
		handleAddUser,
		dataSource,
		user,
		handleTableChange,
		handlePasswordReset,
		handleDeleteUser,
		handleUpdateUser,
	} = props;
	return (
		<>
			<ListUsersFilters
				handleAddUser={handleAddUser}
				handleFilter={handleFilter}
			/>
			<ListUsersTable
				isLoading={isLoading}
				dataSource={dataSource}
				user={user}
				handleTableChange={handleTableChange}
				handlePasswordReset={handlePasswordReset}
				handleDeleteUser={handleDeleteUser}
				handleUpdateUser={handleUpdateUser}
			/>
		</>
	);
};

export default ListUsersScreen;

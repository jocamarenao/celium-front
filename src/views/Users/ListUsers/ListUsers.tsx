import React from 'react';
import moment from 'moment';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import ListUsersScreen from './ListUsersScreen';
import useListUsersHooks from './useListUsersHooks';
import { MDY_FORMAT } from '../../../settings/Dates';
import { UserType, SorterType, userFiltersType } from '../types';

const ListUsers = (props: RouteComponentProps<{}>) => {
	const [
		user,
		users,
		setUserQuery,,
		setDeleteUserParam,,
		alerts,
	] = useListUsersHooks();
	const { history } = props;

	const deleteUser = async (userID: number, username: string) => {
		const confirmation = {
			translations: {
				title: 'Delete User',
				message: `Are you sure you want to delete the user<br> <b>${username}</b>? <br><br>This action cannot be undone.`,
			},
			options: {
				confirmButtonText: 'Delete',
				cancelButtonText: 'Cancel',
			},
			isHtml: true,
		};

		const loading = {
			translations: {
				title: 'Processing',
				message: 'Please wait, this action may take a few seconds to complete',
			},
		};

		alerts.deleteAlert
			.confirmation(
				confirmation.translations.title,
				confirmation.translations.message,
				confirmation.options,
				confirmation.isHtml,
			)
			.then(async () => {
				alerts.loadingAlert.loading(
					loading.translations.title,
					loading.translations.message,
				);
				setDeleteUserParam(userID);
			});
	};

	const dataSource = () => {
		if (!users.data) return [];
		return users.data.map((item: UserType) => ({
			key: item.id,
			id: item.id,
			firstName: item.firstName,
			username: item.username,
			email: item.email,
			role: item.role,
			status: item.status,
			createdAt: moment(item.createdAt).format(MDY_FORMAT),
		}));
	};

	const handleFilter = async (payload: userFiltersType) => {
		setUserQuery(payload);
	};

	const handleTableChange = async (
		pagination: unknown,
		filters: unknown,
		sorter: SorterType,
	) => {
		const { field, order } = sorter;
		setUserQuery({ field, value: order }, 'sort');
	};

	return (
		<ListUsersScreen
			handleFilter={handleFilter}
			isLoading={users.isLoading}
			dataSource={dataSource()}
			user={user}
			handleTableChange={handleTableChange}
			handleAddUser={() => history.push('/users/create')}
			handlePasswordReset={(id, role) => history.push(`/users/password-reset/${id}`, { role })}
			handleDeleteUser={(id, username) => deleteUser(id, username)}
			handleUpdateUser={(id) => history.push(`/users/update/${id}`)}
		/>
	);
};

export default withRouter(ListUsers);

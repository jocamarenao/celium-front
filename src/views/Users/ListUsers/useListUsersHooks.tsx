import { useCallback, useContext, useEffect } from 'react';
import useFetch from '../../../hooks/useFetch';
import { DELETE_USER, GET_USERS } from '../../../endpoints/endpoints';
import UserContext from '../../../context/UserContext';
import Alert from '../../../util/Alert';
import { SUCCESS } from '../../../settings/Errors';

const useListUsersHooks = () => {
	const [users, setUserQuery, clearUserQuery, , , callGetUsers] = useFetch(
		GET_USERS,
	);
	const [deleteUserResponse, , , , setDeleteUserParam] = useFetch(DELETE_USER);
	const { user } = useContext(UserContext);

	const alert = useCallback(() => new Alert(), []);
	const deleteAlert = new Alert();
	const loadingAlert = new Alert();
	const alerts = { alert, deleteAlert, loadingAlert };

	useEffect(() => {
		if (SUCCESS.includes(deleteUserResponse.status)) {
			alert().toast('success', 'User deleted successfully.');
			callGetUsers();
		}
	}, [alert, callGetUsers, deleteUserResponse]);

	return [
		user,
		users,
		setUserQuery,
		clearUserQuery,
		setDeleteUserParam,
		deleteUserResponse,
		alerts,
	] as const;
};

export default useListUsersHooks;

import React from 'react';
import { Table, Tag } from 'antd';

import IconButton from '../../../../components/buttons/IconButton';
import { UserType, ListUsersTypes } from '../../types';
import { SessionType } from '../../../../types/userTypes';

type ListUsersActionsTypes = {
	row: UserType;
	user: SessionType | undefined;
	handlePasswordReset: (id: number, role: string) => void;
	handleDeleteUser: (id: number, username: string) => void;
	handleUpdateUser: (id: number) => void;
};

const ListUsersActions = (props: ListUsersActionsTypes) => {
	const {
		row,
		user,
		handlePasswordReset,
		handleUpdateUser,
		handleDeleteUser,
	} = props;
	return (
		<>
			<IconButton
				handleOnClick={() => handlePasswordReset(row.id, row.role)}
				dataCY={`userPermissions${row.username}`}
				icon="setting"
				tooltipTitle="Update user's settings"
				styles={{
					marginRight: '5%',
					backgroundColor: '#345F80',
					borderColor: '#345F80',
				}}
			/>
			{(row.role !== 'admin' || (user && user.id === row.id)) && (
				<IconButton
					handleOnClick={() => handlePasswordReset(row.id, row.role)}
					dataCY={`passwordReset${row.username}`}
					icon="lock"
					tooltipTitle="Reset user's password"
					styles={{
						marginRight: '5%',
						backgroundColor: '#FFA714',
						borderColor: '#FFA714',
					}}
				/>
			)}
			<IconButton
				handleOnClick={() => handleUpdateUser(row.id)}
				dataCY={`updateUser-${row.username}`}
				icon="edit"
				tooltipTitle="Update user"
				styles={{
					marginRight: '5%',
					backgroundColor: '#FF6D21',
					borderColor: '#FF6D21',
				}}
			/>
			<IconButton
				handleOnClick={() => handleDeleteUser(row.id, row.username)}
				dataCY={`deleteUser-${row.username}`}
				icon="delete"
				tooltipTitle="Delete user"
				styles={{
					backgroundColor: '#FF372E',
					borderColor: '#FF372E',
				}}
			/>
		</>
	);
};

type ColumnsTypes = {
	user: SessionType | undefined;
	handlePasswordReset: (id: number, role: string) => void;
	handleDeleteUser: (id: number, username: string) => void;
	handleUpdateUser: (id: number) => void;
};

const columns = (props: ColumnsTypes) => {
	const {
		user,
		handlePasswordReset,
		handleDeleteUser,
		handleUpdateUser,
	} = props;
	return [
		{
			title: 'id',
			dataIndex: 'id',
			key: 'id',
			sorter: true,
		},
		{
			title: 'Fullname',
			dataIndex: 'firstName',
			key: 'firstName',
			sorter: true,
		},
		{
			title: 'Username',
			dataIndex: 'username',
			key: 'username',
			sorter: true,
		},
		{
			title: 'Email',
			dataIndex: 'email',
			key: 'email',
			sorter: true,
		},
		{
			title: 'Role',
			dataIndex: 'role',
			key: 'role',
			// eslint-disable-next-line react/display-name
			render: (cell: string) => {
				const color = 'blue';
				return <Tag color={color}>{cell}</Tag>;
			},
			sorter: true,
		},
		{
			title: 'Status',
			dataIndex: 'status',
			key: 'status',
			// eslint-disable-next-line react/display-name
			render: (cell: string) => {
				let color = 'green';
				if (cell === 'inactive') color = 'danger';
				return <Tag color={color}>{cell}</Tag>;
			},
			sorter: true,
		},
		{
			title: 'Created At',
			dataIndex: 'createdAt',
			key: 'createdAt',
			sorter: true,
		},
		{
			title: 'Actions',
			dataIndex: 'actions',
			key: 'actions',
			// eslint-disable-next-line react/display-name
			render: (_cell: string, row: UserType) => (
				<ListUsersActions
					user={user}
					row={row}
					handlePasswordReset={handlePasswordReset}
					handleDeleteUser={handleDeleteUser}
					handleUpdateUser={handleUpdateUser}
				/>
			),
		},
	];
};

const ListUsersTable = ({
	isLoading,
	dataSource,
	user,
	handleTableChange,
	handlePasswordReset,
	handleDeleteUser,
	handleUpdateUser,
}: ListUsersTypes) => (
	<Table
		loading={isLoading}
		dataSource={dataSource}
		columns={columns({
			user,
			handlePasswordReset,
			handleDeleteUser,
			handleUpdateUser,
		})}
		onChange={handleTableChange}
	/>
);

export default ListUsersTable;

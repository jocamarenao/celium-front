import React, { useState } from 'react';
import { Formik } from 'formik';
import {
	Row, Card, Col, Button,
} from 'antd';
import Text from '../../../../components/Text/Text';
import IconButton from '../../../../components/buttons/IconButton';
import { grid } from '../../../../theme/theme';
import { ListUsersFiltersType } from '../../types';

const ListUsersFilters = (props: ListUsersFiltersType) => {
	const [showFilters, setShowFilters] = useState(false);
	const { handleAddUser, handleFilter } = props;
	return (
		<>
			<Row>
				<Col span={24}>
					<IconButton
						handleOnClick={() => setShowFilters((prevState) => !prevState)}
						dataCY="userFilter"
						icon="filter"
						tooltipTitle="User Filters"
						buttonType="dashed"
						styles={{
							float: 'left',
							marginBottom: '1%',
						}}
					/>
					<IconButton
						handleOnClick={() => handleAddUser()}
						dataCY="addUser"
						icon="user-add"
						tooltipTitle="Create user"
						styles={{
							backgroundColor: '#C2CC25',
							borderColor: '#C2CC25',
							float: 'right',
							marginBottom: '1%',
						}}
					/>
				</Col>
			</Row>
			{showFilters && (
				<Row style={{ marginBottom: '2%' }}>
					<Col span={24}>
						<Card title="Filters">
							<Formik
								initialValues={{
									firstName: '',
									username: '',
								}}
								onSubmit={(values) => handleFilter(values)}
							>
								{({
									values,
									setValues,
									handleChange,
									handleBlur,
									handleSubmit,
									isSubmitting,
								}) => (
									<form onSubmit={handleSubmit}>
										<Row
											gutter={[
												grid.gutter.horizontal['2x'],
												grid.gutter.vertical['2x'],
											]}
										>
											<Col span={12}>
												<Text
													name="firstName"
													label="First name"
													placeholder="ex: jhon"
													onBlur={handleBlur}
													handleInputChange={handleChange}
													value={values.firstName}
												/>
											</Col>
											<Col span={12}>
												<Text
													name="username"
													label="Username"
													onBlur={handleBlur}
													handleInputChange={handleChange}
													placeholder="ex: jhon_smith"
													value={values.username}
												/>
											</Col>
										</Row>
										<Row style={{ marginBottom: '2%', float: 'right' }}>
											<Col span={24}>
												<Button
													onClick={() => {
														setValues({ firstName: '', username: '' });
														handleFilter({ firstName: '', username: '' });
													}}
													data-cy="userFiltersReset"
													style={{ float: 'right' }}
													type="dashed"
													disabled={isSubmitting}
												>
													clear
												</Button>
												<Button
													data-cy="userFiltersApply"
													style={{ float: 'right' }}
													type="primary"
													htmlType="submit"
													disabled={isSubmitting}
												>
													apply
												</Button>
											</Col>
										</Row>
									</form>
								)}
							</Formik>
						</Card>
					</Col>
				</Row>
			)}
		</>
	);
};

export default ListUsersFilters;

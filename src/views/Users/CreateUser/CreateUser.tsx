import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import CreateUserScene from './CreateUserScene';
import useCreateUserHooks from './useCreateUserHooks';

const CreateUser = (props: RouteComponentProps<{}>) => {
	const { history } = props;
	const [callCreateUsers, createUsersErrors] = useCreateUserHooks(history);
	return (
		<CreateUserScene
			handleSubmit={callCreateUsers}
			errors={createUsersErrors}
		/>
	);
};

export default withRouter(CreateUser);

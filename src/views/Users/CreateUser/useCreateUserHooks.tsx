import { useEffect, useCallback } from 'react';
import History from 'history';
import useFetch from '../../../hooks/useFetch';
import { SUCCESS } from '../../../settings/Errors';
import { CREATE_USER } from '../../../endpoints/endpoints';
import Alert from '../../../util/Alert';

const useCreateUserHooks = (history: History.History<{}> | string[]) => {
	const [
		createUserResponse,,,

		createUsersErrors,,,

		setCreateUsersPayload,
	] = useFetch(CREATE_USER);

	const alert = useCallback(() => new Alert(), []);

	useEffect(() => {
		if (SUCCESS.includes(createUserResponse.status)) {
			alert().toast('success', 'User created successfully.', {});
			history.push('/users');
		}
	}, [alert, history, createUserResponse]);

	return [setCreateUsersPayload, createUsersErrors] as const;
};

export default useCreateUserHooks;

import React from 'react';
import UserForm from '../components/UserForm';
import { CreateUserSceneType } from './types';

const CreateUserScene = (props: CreateUserSceneType) => {
	const { handleSubmit, errors } = props;
	return <UserForm handleOnSubmit={handleSubmit} errors={errors} />;
};

export default CreateUserScene;

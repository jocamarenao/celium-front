import { Errors } from '../../../types';

export type CreateUserSceneType = {
	errors: Errors;
	handleSubmit: (payload: unknown) => void;
};

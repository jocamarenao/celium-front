import React from 'react';
import {
	Button, Form, Row, Col, Card, Icon,
} from 'antd';
import Text from '../../../components/Text/Text';
import { UserFormProps } from './types';

const UserForm = (props: UserFormProps) => {
	const {
		handleSubmit, values, handleInputChange, errors,
	} = props;
	return (
		<div style={{ marginLeft: '20%', marginRight: '20%' }}>
			<Card>
				<Form onSubmit={handleSubmit}>
					<Text
						name="currentPassword"
						label="Current password"
						type="password"
						prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
						value={values.currentPassword}
						handleInputChange={handleInputChange}
						errors={errors}
					/>
					<Text
						name="newPassword"
						label="New password"
						type="password"
						prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
						value={values.newPassword}
						handleInputChange={handleInputChange}
						errors={errors}
					/>
					<Text
						name="confirmNewPassword"
						label="Confirm new password"
						type="password"
						prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
						value={values.confirmNewPassword}
						handleInputChange={handleInputChange}
						errors={errors}
					/>
					<Row>
						<Col span={24}>
							<Button
								data-cy="reset-password-btn"
								disabled={errors.hasErrors()}
								style={{ float: 'right' }}
								type="primary"
								htmlType="submit"
							>
								Reset password
							</Button>
						</Col>
					</Row>
				</Form>
			</Card>
		</div>
	);
};

export default UserForm;

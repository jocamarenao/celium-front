import { Errors } from '../../../types';

export type UserFormValues = {
	currentPassword: string;
	newPassword: string;
	confirmNewPassword: string;
};

export type UserFormProps = {
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	handleSubmit: (event?: any) => void;
	values: UserFormValues;
	handleInputChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
	errors: Errors;
};

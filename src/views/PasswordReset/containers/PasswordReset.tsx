import React, { useContext } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import useApi from '../../../hooks/useApi';
import { RESET_USERS_PASSWORD } from '../../../endpoints/endpoints';
import useForm from '../../../hooks/useForm';
import PasswordResetForm from '../components/PasswordResetForm';
import { SUCCESS } from '../../../settings/Errors';
import UserContext from '../../../context/UserContext';
import Alert from '../../../util/Alert';
import { PasswordForm } from './types';

const alert = new Alert();

const form: PasswordForm = {
	currentPassword: '',
	newPassword: '',
	confirmNewPassword: '',
};

const PasswordReset = (props: RouteComponentProps<{ userID: string }>) => {
	const { values, handleInputChange, handleSubmit } = useForm(create, form);
	const [createUser, , errors] = useApi(RESET_USERS_PASSWORD);
	const { user } = useContext(UserContext);

	async function create() {
		const { match, history } = props;
		if (user && user.id !== parseInt(match.params.userID, 10)) values.userId = match.params.userID;
		const response = await createUser(values);
		if (SUCCESS.includes(response.status)) {
			alert.toast('success', 'Password reset successfully.');
			if (user && user.role === 'general') {
				history.push('/home');
			} else {
				history.push('/users');
			}
		}
	}

	return (
		<PasswordResetForm
			handleSubmit={handleSubmit}
			values={values}
			handleInputChange={handleInputChange}
			errors={errors}
		/>
	);
};

export default withRouter(PasswordReset);

export type PasswordForm = {
	currentPassword: string;
	newPassword: string;
	confirmNewPassword: string;
};

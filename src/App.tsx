import React, { createContext } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './router/router';
import 'antd/dist/antd.css';
import 'sweetalert2/dist/sweetalert2.css';
import useAuth from './hooks/useAuth';

const App: React.FC<{}> = (): JSX.Element => {
	const [, state] = useAuth();
	const AuthenticationContext = createContext({ ...state });
	return (
		<Router>
			<AuthenticationContext.Provider value={state}>
				<Routes />
			</AuthenticationContext.Provider>
		</Router>
	);
};

export default App;

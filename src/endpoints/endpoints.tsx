export const LOGIN = {
	method: 'post',
	endpoint: () => '/authentication/login',
	call: false,
};
export const LOGOUT = { method: 'post', endpoint: () => 'logout', call: false };
export const FETCH_USER = {
	method: 'get',
	endpoint: () => '/authorization/user',
	call: true,
};

export const GET_USERS = {
	method: 'get',
	endpoint: () => '/users',
	call: true,
};
export const GET_USER = {
	method: 'get',
	endpoint: (userID: number) => `/users/${userID}`,
	call: false,
};
export const CREATE_USER = {
	method: 'post',
	endpoint: () => '/users',
	call: false,
};
export const UPDATE_USER = {
	method: 'patch',
	endpoint: (userID: number) => `/users/${userID}`,
	call: false,
};
export const DELETE_USER = {
	method: 'delete',
	endpoint: (userID: number) => `/users/${userID}`,
	call: false,
};
export const RESET_USERS_PASSWORD = {
	method: 'patch',
	endpoint: () => '/authorization/resetPassword',
	call: false,
};

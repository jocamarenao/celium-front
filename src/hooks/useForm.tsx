/* eslint-disable  @typescript-eslint/no-explicit-any */
import React, { useState } from 'react';

const useForm = (onSubmit: () => void, initial: any) => {
	const initialState = () => initial;

	const [values, setValues] = useState(initialState());

	const handleSubmit = (event: React.FormEvent) => {
		if (event) event.preventDefault();
		onSubmit();
	};
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	const handleInputChange = (event: any): void => {
		event.persist();
		setValues((payload: any) => ({
			...payload,
			[event.target.name]: event.target.value,
		}));
	};

	const handleSelectChange = (name: string, value: string): void => {
		setValues((payload: any) => ({ ...payload, [name]: value }));
	};

	const clearValues = (): void => {
		setValues({ ...initialState() });
	};
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	const updateState = (newState: any): void => {
		setValues({ ...newState });
	};

	return {
		handleSubmit,
		handleInputChange,
		values,
		clearValues,
		handleSelectChange,
		updateState,
	};
};
export default useForm;

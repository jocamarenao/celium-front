import { useReducer } from 'react';
import Req from '../util/AxiosRequest';
import { UNPROCESSABLE, SUCCESS } from '../settings/Errors';
import { UseApiState } from './types';

/* eslint-disable  @typescript-eslint/no-explicit-any */
const data: any = new Req();

/* eslint-disable  @typescript-eslint/no-explicit-any */
const reducer = (state: UseApiState, action: any) => {
	switch (action.type) {
	case 'INIT':
		return {
			...state,
			status: null,
			isLoading: true,
			hasData: false,
			errors: [],
		};
	case 'SUCCESS':
		return {
			...state,
			isLoading: false,
			status: 200,
			hasData: Array.isArray(action.payload)
				? !!action.payload.length
				: action.payload,
			data: action.payload,
		};
	case 'FAILURE':
		return {
			...state,
			isLoading: false,
			status: action.payload && action.payload.status,
		};
	case 'CATCH_ERRORS':
		return {
			...state,
			errors: action.payload && action.payload.errors,
			status: action.payload && action.payload.status,
			isLoading: false,
		};
	case 'CLEAR':
		return {
			...state,
			errors: action.payload.errors,
		};
	default:
		throw new Error();
	}
};

interface Url {
	method: string;
	endpoint: (payload?: any) => object | string;
}

const initialReducer = {
	status: null,
	isLoading: false,
	hasData: false,
	data: [],
	errors: [],
};

function useApi(url: Url) {
	const [apiState, dispatch] = useReducer(reducer, initialReducer);

	const api: any = async (payload?: any, uri?: string, query?: string) => {
		dispatch({ type: 'INIT' });
		const { method, endpoint } = url;
		try {
			const response = await data[method](
				`${endpoint(uri)}${query || ''}`,
				payload,
			);
			if (UNPROCESSABLE.includes(response.status)) {
				dispatch({
					type: 'CATCH_ERRORS',
					payload: { status: response.status, errors: response.errors },
				});
			}
			if (SUCCESS.includes(response.status)) dispatch({ type: 'SUCCESS', payload: response.data });
			if (!SUCCESS.includes(response.status)) dispatch({ type: 'FAILURE', payload: { status: response.status } });
			return response;
		} catch (errors) {
			const response = {
				status: errors.status,
				errors: UNPROCESSABLE.includes(errors.status) ? errors.data.errors : [],
			};
			if (!UNPROCESSABLE.includes(response.status)) dispatch({ type: 'FAILURE', payload: { status: response.status } });
			else if (UNPROCESSABLE.includes(response.status)) {
				dispatch({
					type: 'CATCH_ERRORS',
					payload: { status: response.status, errors: response.errors },
				});
			}
			return errors;
		}
	};

	const hasError: any = (field: string) => {
		const { errors } = apiState;
		return errors.some((error: any) => error.param === field);
	};

	const hasErrors: any = () => {
		const { errors } = apiState;
		return !!errors.length;
	};

	const getErrors: any = (field: string) => {
		const { errors } = apiState;
		const newErrors = errors.filter((error: any) => error.param === field);
		if (newErrors.length) return newErrors[0].errors;
		return [];
	};

	const clearError: any = (field?: string) => {
		if (field) {
			const { errors } = apiState;
			dispatch({
				type: 'CLEAR',
				payload: {
					errors: errors.filter((error: any) => error.param !== field),
				},
			});
		} else if (!field) {
			dispatch({ type: 'CLEAR', payload: { errors: [] } });
		}
	};

	const errorsHelpers = {
		hasError,
		hasErrors,
		getErrors,
		clearError,
	};

	return [api, apiState, errorsHelpers];
}

export default useApi;

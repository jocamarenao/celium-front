import React from 'react';
import useLocalStorage from './useLocalStorage';
import useApi from './useApi';
import { FETCH_USER } from '../endpoints/endpoints';
import { UseAuthAction, UseAuthState, LoginPayload } from './types';

const initialState: UseAuthState = {
	isAuthenticated: false,
	user: {},
};

const reducer = (state: UseAuthState, action: UseAuthAction): UseAuthState => {
	switch (action.type) {
	case 'LOGIN':
		return {
			...state,
			isAuthenticated: true,
			user: action.payload && action.payload.user,
		};
	case 'LOGOUT':
		return {
			...state,
			isAuthenticated: false,
			user: {},
		};
	default:
		throw new Error();
	}
};

const useAuth = () => {
	const [state, dispatch] = React.useReducer(reducer, initialState);
	const [fetchUser, user] = useApi(FETCH_USER);
	const [set] = useLocalStorage();

	const login = async (payload: LoginPayload) => {
		try {
			set('token', payload.token);
			set('expiresAt', payload.expiresAt);
			await fetchUser();
			dispatch({ type: 'LOGIN', payload: { user } });
			window.location.replace('/');
		} catch (e) {
			dispatch({ type: 'LOGOUT' });
		}
	};

	return [login, state];
};

export default useAuth;

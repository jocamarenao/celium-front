/* eslint-disable  @typescript-eslint/no-explicit-any */
import { useReducer } from 'react';
import { UseDataType } from './types';
import { Errors } from '../types';

const reducer = (state: UseDataType, action: any) => {
	switch (action.type) {
	case 'INIT':
		return {
			...state,
			status: null,
			isLoading: true,
			hasData: false,
			errors: [],
		};
	case 'SUCCESS':
		return {
			...state,
			isLoading: false,
			status: 200,
			hasData: Array.isArray(action.payload)
				? !!action.payload.length
				: action.payload,
			data: action.payload,
		};
	case 'FAILURE':
		return {
			...state,
			isLoading: false,
			status: action.payload && action.payload.status,
		};
	case 'CATCH_ERRORS':
		return {
			...state,
			errors: action.payload && action.payload.errors,
			status: action.payload && action.payload.status,
			isLoading: false,
		};
	case 'CLEAR':
		return {
			...state,
			errors: action.payload.errors,
		};
	default:
		throw new Error();
	}
};

const initialReducer = {
	status: null,
	isLoading: false,
	hasData: false,
	data: [],
	errors: [],
};

const useData = () => {
	const [state, dispatch] = useReducer(reducer, initialReducer);

	const hasError: any = (field: string) => {
		const { errors } = state;
		return errors.some((error: any) => error.param === field);
	};

	const hasErrors: any = () => {
		const { errors } = state;
		return !!errors.length;
	};

	const getErrors: any = (field: string) => {
		const { errors } = state;
		const newErrors = errors.filter((error: any) => error.param === field);
		if (newErrors.length) return newErrors[0].errors;
		return [];
	};

	const clearError: any = (field?: string) => {
		if (field) {
			const { errors } = state;
			dispatch({
				type: 'CLEAR',
				payload: {
					errors: errors.filter((error: any) => error.param !== field),
				},
			});
		} else if (!field) {
			dispatch({ type: 'CLEAR', payload: { errors: [] } });
		}
	};

	const errorsHelpers: Errors = {
		hasError,
		hasErrors,
		getErrors,
		clearError,
	};

	return [state, dispatch, errorsHelpers] as const;
};

export default useData;

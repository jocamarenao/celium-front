/* eslint-disable  @typescript-eslint/no-explicit-any */
import { useCallback, useState, useEffect } from 'react';

type QueryType = {
	page: number;
	sort: {
		field: string;
		order: string;
	};
	filters: any;
};

type URLType = {
	method: string;
	endpoint: (payload?: any) => object | string;
	call: boolean;
};

type InitialRequestType = URLType & {
	payload: unknown;
	query: QueryType;
	uri: string;
	param: string | number;
};

const initialQueryData: InitialRequestType = {
	method: '',
	endpoint: () => '',
	call: false,
	param: '',
	payload: {},
	uri: '',
	query: {
		page: 1,
		sort: {
			field: '',
			order: '',
		},
		filters: {},
	},
};

const processQuery = (query: QueryType) => {
	let payload = `?page=${query.page}`;
	if (query.sort.field) payload += `&sort=${query.sort.field}:${query.sort.order}`;
	Object.keys(query.filters).forEach((key) => {
		if (query.filters[key]) payload += `&${key}=${query.filters[key]}`;
	});
	return payload;
};

const useRequestData = (url: URLType) => {
	const [requestData, setRequestData] = useState(initialQueryData);

	useEffect(() => {
		setEndpoint(url);
	}, [url]);

	const setQuery = (data: any, type = 'filter') => {
		if (type === 'sort') {
			if (data.value) {
				setRequestData((prevState) => ({
					...prevState,
					uri: processQuery({
						...prevState.query,
						sort: { field: data.field, order: data.value },
					}),
					query: {
						...prevState.query,
						sort: { field: data.field, order: data.value },
					},
				}));
			}
			if (!data.value) {
				setRequestData((prevState) => ({
					...prevState,
					uri: processQuery({
						...prevState.query,
						sort: { field: '', order: '' },
					}),
					query: { ...prevState.query, sort: { field: '', order: '' } },
				}));
			}
		}
		if (type === 'filter') {
			setRequestData((prevState) => ({
				...prevState,
				uri: processQuery({ ...prevState.query, filters: data }),
				query: { ...prevState.query, filters: data },
			}));
		}
	};

	const clearQuery = () => setRequestData((prevState) => ({
		...prevState,
		query: { ...prevState.query, filters: {} },
	}));

	const setPayload = (payload: unknown) => {
		setRequestData((prevState) => ({ ...prevState, payload, call: true }));
	};

	const setParam = (param: string | number) => {
		setRequestData((prevState) => ({ ...prevState, param, call: true }));
	};

	const setData = (payload: unknown, param: string | number) => {
		setRequestData((prevState) => ({
			...prevState,
			param,
			payload,
			call: true,
		}));
	};

	const callRequest = useCallback(() => {
		setRequestData((prevState) => ({ ...prevState, call: true }));
	}, []);

	const setEndpoint = (payload: URLType) => {
		const { method, endpoint, call } = payload;
		setRequestData((prevState) => ({
			...prevState,
			method,
			endpoint,
			call,
		}));
	};

	return [
		requestData,
		setQuery,
		clearQuery,
		setParam,
		callRequest,
		setPayload,
		setData,
	] as const;
};

export default useRequestData;

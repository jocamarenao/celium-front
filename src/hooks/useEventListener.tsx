import { useRef, useEffect } from 'react';

const useEventListener = (
	eventName: string,
	handler: () => void,
	element = window,
) => {
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	const savedHandler: any = useRef();

	useEffect(() => {
		savedHandler.current = handler;
	}, [handler]);

	useEffect(() => {
		const isSupported = element && element.addEventListener;

		if (!isSupported) return;
		/* eslint-disable  @typescript-eslint/no-explicit-any */
		const eventListener: any = (event: any) => savedHandler.current(event);

		element.addEventListener(eventName, eventListener);

		element.removeEventListener(eventName, eventListener);
	}, [eventName, element]);
};

export default useEventListener;

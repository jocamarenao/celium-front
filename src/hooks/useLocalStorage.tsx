import { useCallback } from 'react';

/* eslint-disable  @typescript-eslint/no-explicit-any */
const useLocalStorage = () => {
	const set = useCallback(
		(key: any, value: any): any => localStorage.setItem(key, value),
		[],
	);
	const get = (key: any): any => localStorage.getItem(key) || null;
	const remove = (key: any): any => localStorage.removeItem(key);
	return [set, get, remove];
};

export default useLocalStorage;

import { useRef, useEffect } from 'react';

/* eslint-disable  @typescript-eslint/no-explicit-any */
const useEventListener = (value: any) => {
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	const ref: any = useRef();

	useEffect(() => {
		ref.current = value;
	});
	return ref.current;
};

export default useEventListener;

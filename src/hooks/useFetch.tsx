/* eslint-disable  @typescript-eslint/no-explicit-any */
import { useEffect } from 'react';
import Req from '../util/AxiosRequest';
import { UNPROCESSABLE, SUCCESS } from '../settings/Errors';
import useData from './useData';
import useRequestData from './useRequestData';

const data: any = new Req();

type URLType = {
	method: string;
	endpoint: (payload?: any) => object | string;
	call: boolean;
};

const useFetch = (url: URLType) => {
	const [
		requestData,
		setQuery,
		clearQuery,
		setParam,
		callRequest,
		setPayload,
		setData,
	] = useRequestData(url);
	const [state, dispatch, errorsHelpers] = useData();

	useEffect(() => {
		const api: any = async () => {
			dispatch({ type: 'INIT' });
			try {
				const { method, endpoint } = url;
				const response = await data[method](
					`${endpoint(requestData.param)}${requestData.uri}`,
					requestData.payload,
				);
				if (UNPROCESSABLE.includes(response.status)) {
					dispatch({
						type: 'CATCH_ERRORS',
						payload: { status: response.status, errors: response.errors },
					});
				}
				if (SUCCESS.includes(response.status)) dispatch({ type: 'SUCCESS', payload: response.data });
				if (!SUCCESS.includes(response.status)) dispatch({ type: 'FAILURE', payload: { status: response.status } });
				return response;
			} catch (errors) {
				const response = {
					status: errors.status,
					errors: UNPROCESSABLE.includes(errors.status)
						? errors.data.errors
						: [],
				};
				if (!UNPROCESSABLE.includes(response.status)) dispatch({ type: 'FAILURE', payload: { status: response.status } });
				else if (UNPROCESSABLE.includes(response.status)) {
					dispatch({
						type: 'CATCH_ERRORS',
						payload: { status: response.status, errors: response.errors },
					});
				}
				return errors;
			}
		};

		if (requestData.call) api();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [url, requestData]);

	return [
		state,
		setQuery,
		clearQuery,
		errorsHelpers,
		setParam,
		callRequest,
		setPayload,
		setData,
	] as const;
};

export default useFetch;

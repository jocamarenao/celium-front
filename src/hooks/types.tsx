export type UseApiState = {
	isLoading: boolean | undefined;
	hasData: boolean | undefined;
	data: object[] | undefined;
	errors: object[] | undefined;
	status: number | null | undefined;
};

export type UseDataType = {
	isLoading: boolean;
	hasData: boolean | undefined;
	data: object[] | undefined;
	errors: object[] | undefined;
	status: number | null | undefined;
};

export type Payload = {
	errors: object[];
	status: number | null;
};

export type UseApiAction = {
	type: 'INIT' | 'SUCCESS' | 'FAILURE' | 'CLEAR' | 'CATCH_ERRORS';
	payload?: Payload;
};

export type UseAuthState = {
	isAuthenticated: boolean;
	user: object | undefined;
};

export type UseAuthPayload = {
	user: object;
};

export type UseAuthAction = {
	type: 'LOGIN' | 'LOGOUT';
	payload?: UseAuthPayload;
};

export type LoginPayload = {
	token: string;
	expiresAt: string;
};

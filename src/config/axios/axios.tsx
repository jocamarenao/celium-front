/* eslint-disable  @typescript-eslint/no-explicit-any */
import axios from 'axios';
import {
	AUTH_ERRORS,
	REDIRECT_ERRORS,
	COMMON_ERRORS,
} from '../../settings/Errors';
import { DATE_RANGE_FILTER } from '../../settings/Dates';
import ENVIRONMENT from '../enviroment';
import Alert from '../../util/Alert';

const alert = new Alert();
const configInstance = { baseURL: ENVIRONMENT.BASE_URL };

const instances = { api: axios.create(configInstance) };

function getToken() {
	let authToken: string | null = '';
	authToken = localStorage.getItem('token');
	return authToken;
}

function handleError(error: any) {
	const { status } = error;
	if (AUTH_ERRORS.includes(status)) {
		localStorage.removeItem('token');
	}
	if (REDIRECT_ERRORS.includes(status)) {
		window.location.replace('/home');
	}
	if (COMMON_ERRORS.includes(status)) {
		alert.toast('error', `Error with status ${status}`);
	}
}

export const getURLParams = function getURLParams(query: any) {
	const payload = query;
	if (typeof query !== 'object') {
		return '';
	}
	if (query[DATE_RANGE_FILTER] && query[DATE_RANGE_FILTER] === 'custom') {
		delete payload[DATE_RANGE_FILTER];
	}
	const response = Object.keys(query)
		.map((item) => `${item}=${encodeURIComponent(payload[item])}`)
		.join('&');

	return `${!response || response.includes('?') ? response : `?${response}`}`;
};

instances.api.interceptors.request.use(
	(config) => {
		const authToken = getToken();
		const payload = config;
		if (typeof authToken === 'string' && authToken.length) {
			payload.headers.authorization = `Bearer ${authToken}`;
		}
		return payload;
	},
	(error) => Promise.reject(error),
);

instances.api.interceptors.response.use(
	(response: any) => response,
	(error) => {
		handleError(error.response);
		return Promise.reject(error.response);
	},
);

export const { api } = instances;
export default instances;

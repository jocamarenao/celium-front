const development = { BASE_URL: 'http://localhost:3001/api/v1/' };

const test = { BASE_URL: 'https://celium-api.codeville.co/api/v1/' };

export const config = process.env.REACT_APP_STAGE === 'development' ? development : test;

export default {
	AUTH_STORE: 'COOKIE',
	LOGOUT_TIME: 900000,
	...config,
};

/* eslint-disable  @typescript-eslint/no-explicit-any */
import React from 'react';
import { SessionType } from '../types/userTypes';

export type LoginPayload = {
	token: string;
	tokenExpiresAt: string;
};

export type ContextProps = {
	isAuthenticated: boolean;
	user: SessionType;
	handleLogin: (payload: LoginPayload, history: any) => void;
	handleLogout: () => void;
	loading: boolean;
};

export default React.createContext<Partial<ContextProps>>({});

export type SessionType = {
	id: number;
	role: string;
};

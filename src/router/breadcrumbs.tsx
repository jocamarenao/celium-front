const breadcrumbNameMap: Record<string, string> = {
	'/users': 'Users',
	'/users/create': 'Create',
	'/users/update': 'Update',
	'/users/password-reset': 'Password Reset',
};

export default breadcrumbNameMap;

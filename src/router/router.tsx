import React from 'react';
import {
	withRouter,
	Switch,
	Route,
	RouteComponentProps,
} from 'react-router-dom';
import ProtectedRoute from '../components/ProtectedRoute/ProtectedRoute';
import components from './components';
import UserContextProvider from '../components/UserContextProvider/UserContextProvider';

const Routes = (props: RouteComponentProps<{}>): JSX.Element => {
	const getRoutes = (): JSX.Element[] => {
		const routes: JSX.Element[] = [];
		components.forEach((component) => {
			if (component.protected && component.name) {
				routes.push(
					<ProtectedRoute
						key={component.name}
						exact={component.exact}
						path={component.path}
						component={component.component}
						name={component.name}
					/>,
				);
			} else if (!component.protected) {
				routes.push(
					<Route
						key={component.name}
						exact={component.exact}
						path={component.path}
						component={component.component}
					/>,
				);
			}
		});
		return routes;
	};

	return (
		<UserContextProvider>
			<Switch>{getRoutes()}</Switch>
		</UserContextProvider>
	);
};

export default withRouter(Routes);

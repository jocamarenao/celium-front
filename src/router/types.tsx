import { RouteComponentProps } from 'react-router-dom';

export type RouteProps = {
	location: RouteComponentProps;
};

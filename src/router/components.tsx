import Home from '../views/Home/Home';
import ListUsers from '../views/Users/ListUsers/ListUsers';
import PasswordReset from '../views/PasswordReset/containers/PasswordReset';
import CreateUser from '../views/Users/CreateUser/CreateUser';
import UpdateUser from '../views/Users/UpdateUser/UpdateUser';
import Logout from '../components/Logout/Logout';
import Auth from '../views/Auth/Auth';
import NotFound from '../components/NotFound/NotFound';

export default [
	{
		path: '/home',
		exact: true,
		component: Home,
		name: 'Home',
		protected: true,
	},
	{
		path: '/users',
		exact: true,
		component: ListUsers,
		name: 'Users',
		protected: true,
	},
	{
		path: '/users/create',
		exact: true,
		component: CreateUser,
		name: 'Create User',
		protected: true,
	},
	{
		path: '/users/update/:userID',
		exact: true,
		component: UpdateUser,
		name: 'Update User',
		protected: true,
	},
	{
		path: '/users/password-reset/:userID',
		exact: true,
		component: PasswordReset,
		name: 'User Password Reset',
		protected: true,
	},
	{
		path: '/auth',
		exact: false,
		component: Auth,
		name: 'Auth',
		protected: false,
	},
	{
		path: '/logout',
		exact: true,
		component: Logout,
		name: 'Logout',
		protected: true,
	},
	{
		path: '/',
		exact: false,
		component: NotFound,
		name: 'NotFound',
		protected: false,
	},
];

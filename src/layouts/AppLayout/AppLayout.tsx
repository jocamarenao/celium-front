import React, { useState, useContext } from 'react';
import { Layout } from 'antd';
import ENVIRONMENT from '../../config/enviroment';
import SideMenu from './components/SideMenu/SideMenu';
import AppContent from './components/AppContent/AppContent';
import AppHeader from './components/AppContent/AppHeader/AppHeader';
import useEventListener from '../../hooks/useEventListener';
import UserContext from '../../context/UserContext';
import { AppLayoutProps } from './types';

const {
	Header, Footer, Sider, Content,
} = Layout;

const AppLayout = (props: AppLayoutProps) => {
	const { handleLogout } = useContext(UserContext);
	/* eslint-disable  @typescript-eslint/no-explicit-any */
	const [timer, setTimer] = useState<any>(null);
	const [logoutTime] = useState<number>(ENVIRONMENT.LOGOUT_TIME);
	const [collapsed, setCollapsed] = useState<boolean>(false);

	const resetTimer = () => {
		window.clearTimeout(timer);
		const timeout = window.setTimeout(() => {
			if (handleLogout) handleLogout();
		}, logoutTime);
		setTimer(timeout);
	};

	useEventListener('mousemove', resetTimer);
	useEventListener('mousedown', resetTimer);
	useEventListener('click', resetTimer);
	useEventListener('scroll', resetTimer);
	useEventListener('keypress', resetTimer);

	return (
		<Layout style={{ height: '100vh' }}>
			<Sider
				breakpoint="lg"
				trigger={null}
				collapsedWidth={0}
				collapsed={collapsed}
			>
				<SideMenu />
			</Sider>
			<Layout>
				<Header style={{ background: '#fff', padding: 0 }}>
					<AppHeader
						collapsed={collapsed}
						toggle={() => setCollapsed(!collapsed)}
					/>
				</Header>
				<Content style={{ overflow: 'initial', margin: '6px 4px 0' }}>
					<AppContent {...props} />
				</Content>
				<Footer style={{ textAlign: 'center' }}>
					Celium ©2019 Created by Codeville
				</Footer>
			</Layout>
		</Layout>
	);
};

export default AppLayout;

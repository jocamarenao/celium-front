export type AppLayoutProps = {
	children: object;
	name: string;
};

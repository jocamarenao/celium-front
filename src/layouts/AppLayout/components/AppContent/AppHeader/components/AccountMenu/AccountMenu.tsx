/* eslint-disable */
import React, { useContext } from "react";
import { Menu, Dropdown, Icon, Avatar } from "antd";
import { Link } from "react-router-dom";
import UserContext from "../../../../../../../context/UserContext";

const AccountMenu = () => {
	const { user } = useContext(UserContext);
	let userId = null;
	if (user && user.role === "general") {
		userId = user.id;
	}

	const menu = (
		<Menu style={{ marginTop: 10 }}>
			{user && user.role === "general" ? (
				<Menu.Item>
					<Link to={`/users/password-reset/${userId}`}>Reset Password</Link>
				</Menu.Item>
			) : null}

			<Menu.Item>
				<Link to="/logout">Logout</Link>
			</Menu.Item>
		</Menu>
	);
	return (
		<div style={{ float: "right", marginRight: "1%" }}>
			<Dropdown overlay={menu} trigger={["click"]} placement="bottomCenter">
				<a className="ant-dropdown-link" href="_blank">
					<Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>
						U
					</Avatar>
					<Icon type="down" />
				</a>
			</Dropdown>
		</div>
	);
};

export default AccountMenu;

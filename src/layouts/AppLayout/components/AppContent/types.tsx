import React from 'react';

export type RoutesProps = {
	children: React.ReactNode;
	name: string;
};

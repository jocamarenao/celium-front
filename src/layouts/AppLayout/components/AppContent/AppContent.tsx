import React from 'react';
import { PageHeader, Card, Breadcrumb } from 'antd';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';
import breadcrumbNameMap from '../../../../router/breadcrumbs';
import styles from './index.module.css';
import { RoutesProps } from './types';

const Routes = (props: RouteComponentProps<{}> & RoutesProps) => {
	const { children, name, location } = props;
	const pathSnippets = location.pathname.split('/').filter((i: string) => i);

	/* eslint-disable  @typescript-eslint/no-explicit-any */
	const extraBreadcrumbItems: any = pathSnippets.map(
		(_: string, index: number) => {
			const from = 0;
			const to = 1;
			const url = `/${pathSnippets.slice(from, index + to).join('/')}`;
			if (!breadcrumbNameMap[url]) return null;
			return (
				<Breadcrumb.Item key={url}>
					<Link to={url}>{breadcrumbNameMap[url]}</Link>
				</Breadcrumb.Item>
			);
		},
	);

	const breadcrumbItems = [
		<Breadcrumb.Item key="home">
			<Link to="/">Home</Link>
		</Breadcrumb.Item>,
	].concat(extraBreadcrumbItems);

	const breads = (
		<>
			<Breadcrumb>{breadcrumbItems}</Breadcrumb>
			{name}
		</>
	);

	return (
		<>
			<div className={styles.pageHeaderColor}>
				<PageHeader
					style={{ background: '#1890FF', margin: 10 }}
					title={breads}
				/>
			</div>
			<Card style={{ background: '#fff', minHeight: 360, margin: 10 }}>
				{children}
			</Card>
		</>
	);
};

export default withRouter(Routes);

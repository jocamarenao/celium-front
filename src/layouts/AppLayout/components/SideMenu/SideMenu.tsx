import React from 'react';
import { Menu, Icon } from 'antd';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';

const SideMenu = (props: RouteComponentProps<{}>) => {
	const { location } = props;
	return (
		<>
			<Menu theme="dark" mode="inline" selectedKeys={[location.pathname]}>
				<Menu.Item key="/home">
					<Icon style={{ display: 'inline' }} type="home" />
					<Link style={{ display: 'inline' }} to="/home">
						Home
					</Link>
				</Menu.Item>
				<Menu.Item key="/users">
					<Icon style={{ display: 'inline' }} type="user" />
					<Link style={{ display: 'inline' }} data-cy="users" to="/users">
						Users
					</Link>
				</Menu.Item>
			</Menu>
		</>
	);
};

export default withRouter(SideMenu);

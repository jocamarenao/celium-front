# Codeville Celium SPA Application

Your starting point for any CMS software project.

## Built With

* [React](https://reactjs.org/) - A JavaScript library for building user interfaces
* [Typescript](https://www.typescriptlang.org/) - TypeScript brings you optional static type-checking along with the latest ECMAScript features.
* [Ant Design](https://ant.design/) - A design system for enterprise-level products. Create an efficient and enjoyable work experience.
* [Cypress](https://www.cypress.io/) - Fast, easy and reliable testing for anything that runs in a browser.

## Modules

* Authentication
    * login
    * logout
* User managenent
    * roles
    * create
    * view
    * update
    * delete
* Permissions

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

* node >= 10.16.3
* npm >= 5.9.0
* npx >= 6.9.0
* jscpd >= latest

## Installing npx

* https://www.npmjs.com/package/npx

## Installing jscpd (make sure is globally)
* https://github.com/kucherenko/jscpd#installation

## Installing Project

In order to run the project you must follow the next steps:

* Execute the installation command (from the root of the project) 
```
source install.sh
```

* This command will:
* Install npm dependancies
* Making the project fully functional.

## Running Project

* Execute the start command

To run project with local api.

```
npm run start
```

* Execute the remote command

To run project with remote api.

```
npm run remote
```

## Uninstalling
If you want to uninstall everything executed by the install.sh command, you can do it using by typing the following command at the root of the project.
```
source uninstall.sh 
```

You will be asked for confirmation before uninstalling the project.

## Quality Assurance tools
In order to keep the code clean, structured and based on certain standards, the following list of tools are implemented.


* [JSCPD](https://github.com/kucherenko/jscpd) - Copy/paste detector for programming source code.
* [Prettier](https://prettier.io/) -An opinionated code formatter Supports many languages Integrates with most editors Has few options
* [ESLint](https://eslint.org/) -An opinionated code formatter Supports many languages Integrates with most editors Has few options


## Reporting

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](). 

## Authors

* **Jonatan Camarena** - [jonatancamarena](https://gitlab.com/jocamarenao)


/// <reference types="Cypress" />

Cypress.Commands.add('login', (email, password) => {
	cy.visit('http://localhost:3000/auth');
	cy.get('[data-cy=input-email]').type(email);
	cy.get('[data-cy=input-password]').type(password);
	cy.get('[type="submit"]').click();
});

Cypress.Commands.add('mutateUser', (user, isCreate = true) => {
	cy.get('[data-cy=select-role]')
		.children('.ant-select-selection__rendered')
		.children('.ant-select-selection-selected-value')
		.click()
		.then(function() {
			cy.get('.ant-select-dropdown').then(function($dropdown) {
				$dropdown[0].setAttribute('removeClass', '.ant-select-dropdown-hidden');
				cy.get('.ant-select-dropdown-menu-item')
					.contains(user.role)
					.click();
			});
		});

	cy.get('[data-cy=input-firstName]').clear();
	cy.get('[data-cy=input-firstName]').type(user.firstName);

	cy.get('[data-cy=input-lastName]').clear();
	cy.get('[data-cy=input-lastName]').type(user.lastName);

	cy.get('[data-cy=input-username]').clear();
	cy.get('[data-cy=input-username]').type(user.username);

	let email = user.email;
	if (!isCreate) {
		email = user.email;
	}
	cy.get('[data-cy=input-email]').clear();
	cy.get('[data-cy=input-email]').type(email);

	if (isCreate) {
		cy.get('[data-cy=input-password]').clear();
		cy.get('[data-cy=input-password]').type(user.password);

		cy.get('[data-cy=input-confirmPassword]').clear();
		cy.get('[data-cy=input-confirmPassword]').type(user.confirmPassword);
	}

	cy.get('[data-cy=UserForm-Button]').click();
});

Cypress.Commands.add('createUser', userData => {
	it('should create a user successfully', () => {
		cy.get('[data-cy=users]').click();
		cy.location('pathname').should('eq', '/users');

		cy.get('[data-cy=addUser]').click();
		cy.location('pathname').should('eq', '/users/create');

		cy.mutateUser(userData);
		cy.location('pathname').should('eq', '/users');
	});
});

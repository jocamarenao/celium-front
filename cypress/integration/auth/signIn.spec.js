/// <reference types="Cypress" />

context('Auth', function() {
	describe('Test Sign in', () => {
		it('should sign in successfully', () => {
			cy.login('camarena.dev@gmail.com', '123123');
			cy.location('pathname').should('eq', '/home');
		});
		it('should show wrong credentials', () => {
			cy.login('camarena.dev@gmail.com', '12312');
			cy.get('[data-cy=wrongCredentials]').should('be.visible');
		});
		it('should shave values ', () => {
			cy.visit('http://localhost:3000/auth');
			cy.get('[type="submit"]').click();
			cy.get('.ant-form-explain').contains('The email is required');
			cy.get('.ant-form-explain').contains('The password is required');
		});
	});
});

/// <reference types="Cypress" />

context('Users', function () {
	beforeEach(function () {
		cy.fixture('userInputs').then(data => {
			this.userData = data;
		});
	});

	describe('Test User module', function () {
		it('should create a user successfully', function () {
			const { createUser } = this.userData;

			cy.login('camarena.dev@gmail.com', '123123');
			cy.location('pathname').should('eq', '/home');
			cy.wait(1000);

			cy.get('[data-cy=users]').dblclick();
			cy.location('pathname').should('eq', '/users');

			cy.get('[data-cy=addUser]').click();
			cy.location('pathname').should('eq', '/users/create');

			cy.mutateUser(createUser);
			cy.location('pathname').should('eq', '/users');
		});

		it('should update a user successfully', function () {
			const { createUser, updateUser } = this.userData;

			cy.login('camarena.dev@gmail.com', '123123');
			cy.location('pathname').should('eq', '/home');
			cy.wait(1000);

			cy.get('[data-cy=users]').dblclick();
			cy.location('pathname').should('eq', '/users');

			cy.get(`[data-cy=updateUser-${createUser.username}]`).click();
			cy.location('pathname').should('contains', '/users/update/');

			cy.mutateUser(updateUser, false);
			cy.location('pathname').should('eq', '/users');
		});

		it('should delete a users successfully', function () {
			const { updateUser } = this.userData;

			cy.login('camarena.dev@gmail.com', '123123');
			cy.location('pathname').should('eq', '/home');
			cy.wait(1000);

			cy.get('[data-cy=users]').dblclick();
			cy.location('pathname').should('eq', '/users');

			cy.get(`[data-cy=deleteUser-${updateUser.username}]`).click();
			cy.get('.swal2-header').should('exist');

			cy.get('.swal2-confirm').click();
			cy.get('.swal2-success').should('exist');
		});
	});
});
